const express = require("express");
require("dotenv").config("../.env");
const app = express();
const errorMiddleware = require("./error");
const morgan = require("morgan");
const cors = require("cors");
const cookieParser = require("cookie-parser");

app.use(cookieParser());
app.use([
    // morgan("dev"),
    cors({
        origin: [
            "http://localhost:3000",
            "https://planpal-fardinoa.vercel.app",
            "https://planpal-git-main-fardinoa.vercel.app",
            "https://planpal-one.vercel.app",
        ],
        credentials: true,
    }),
    express.json(),
]);

const userRouter = require("../routes/userRoute");
const taskRoute = require("../routes/taskRoute");

app.use("/plan/v1", userRouter, taskRoute);

app.get("/", (_req, res) => {
    res.status(200).json({ message: "from the root" });
});

// app.use((req, res, next) => {
//     const error = new Error("Resource Not Found");
//     error.status = 404;
//     next(error);
// });

// app.use((error, req, res, next) => {
//     if (error.status) {
//         return res.status(error.status).json({
//             message: error.message,
//         });
//     }

//     res.status(500).json({
//         message: "Something Went Wrong",
//     });
// });
app.use(errorMiddleware);
module.exports = app;
