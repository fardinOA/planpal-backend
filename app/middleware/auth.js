const ErrorHandeler = require("../errorHandeler");
const catchAssyncErrors = require("../catchAssyncErrors");
const jwt = require("jsonwebtoken");
const User = require("../../models/userModel");
const isAuth = catchAssyncErrors(async (req, res, next) => {
    const { token } = req.cookies;

    if (!token) {
        return next(
            new ErrorHandeler("Please login to access this resource", 401)
        );
    }

    const decodedData = jwt.verify(token, process.env.JWT_SECRET);

    req.user = await User.findById(decodedData.id);

    next();
});

module.exports = { isAuth };
