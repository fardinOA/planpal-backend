const http = require("http");
const app = require("./app/app");
const mongoose = require("mongoose");

const server = http.createServer(app);
process.on("uncaughtException", (err) => {
    console.log(`Error: ${err.message}`);
    console.log(`Shutting down the server due to uncaught Error exceptions`);

    process.exit(1);
});
const PORT = process.env.PORT || 8000;

mongoose
    .connect(
        `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0-shard-00-00.8a7eo.mongodb.net:27017,cluster0-shard-00-01.8a7eo.mongodb.net:27017,cluster0-shard-00-02.8a7eo.mongodb.net:27017/${process.env.DB_NAME}?ssl=true&replicaSet=atlas-dr9kgz-shard-0&authSource=admin&retryWrites=true&w=majority`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        }
    )
    .then(() => {
        console.log("database connected");
        server.listen(PORT, () => {
            console.log(`Running on Port ${PORT} `);
        });
    })
    .catch((e) => {
        console.log(e);
    });

process.on("unhandledRejection", (err) => {
    console.log(`Error: ${err.message}`);
    console.log(`Shutting down the server due to unhandled promise rejection`);
    server.close(() => {
        process.exit(1);
    });
});
