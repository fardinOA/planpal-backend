
# Plan Pal Backend

Plan pal is a modern todo application.



## Run

Install the project with yarn

```bash
 https://gitlab.com/fardinOA/planpal-backend.git
```

```bash
  cd planpal-backend
```

```bash
  yarn
```
```bash
  yarn dev
```

    
## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`MONGO_USER`
`MONGO_PASSWORD`
`DB_NAME`
`JWT_EXPIRE`
`COOKIE_EXPIRE`
`JWT_SECRET`

## Support

For support, email afnan.eu.cse@gmail.com

