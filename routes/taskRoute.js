const express = require("express");

const { isAuth } = require("../app/middleware/auth");
const {
    createTask,
    getAllTasks,
    getFilteredTasks,
    taskDone,
    deleteTask,
} = require("../controller/taskController");
const router = express.Router();

router.post("/create-task", isAuth, createTask);
router.get("/tasks", isAuth, getAllTasks);
router.put("/task/:id", isAuth, taskDone);
router.delete("/task/:id", isAuth, deleteTask);
router.get("/filtered-tasks", isAuth, getFilteredTasks);

module.exports = router;
