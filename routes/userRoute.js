const express = require("express");
const {
    register,
    getUserInfo,
    login,
    logout,
} = require("../controller/userController");
const { isAuth } = require("../app/middleware/auth");
const router = express.Router();

router.post("/register", register);
router.post("/login", login);
router.get("/me", isAuth, getUserInfo);
router.get("/logout", logout);

module.exports = router;
