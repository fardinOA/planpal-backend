const catchAssyncErrors = require("../app/catchAssyncErrors");
const ErrorHandeler = require("../app/errorHandeler");
const User = require("../models/userModel");
const sendToken = require("../utils/sendLoginToken");

exports.register = catchAssyncErrors(async (req, res, next) => {
    const user = await User.create(req.body);

    if (!user) return next(new ErrorHandeler("Can't create a user", 424));
    sendToken(user, 201, res);
});

exports.login = catchAssyncErrors(async (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password) {
        return next(new ErrorHandeler("Please Enter Email & Password", 400));
    }

    const user = await User.findOne({
        email,
    });

    if (!user) {
        return next(new ErrorHandeler("Invalid Email or Password", 400));
    }

    if (!(await user.comparePassword(password))) {
        return next(new ErrorHandeler("Invalid Email or Password", 401));
    }

    sendToken(user, 200, res);
});

// get my{for all login users} info
exports.getUserInfo = catchAssyncErrors(async (req, res, next) => {
    const user = await User.findById(req.user.id);

    res.status(200).json({
        success: true,
        user,
    });
});

exports.logout = catchAssyncErrors(async (req, res, next) => {
    res.cookie("token", null, {
        expires: new Date(Date.now()),
        httpOnly: true,
        sameSite: "None",
        secure: true,
    });
    res.status(200).json({
        success: true,
        message: "User logged out",
    });
});
