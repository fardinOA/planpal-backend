const catchAssyncErrors = require("../app/catchAssyncErrors");
const ErrorHandeler = require("../app/errorHandeler");
const Task = require("../models/taskModel");

exports.createTask = catchAssyncErrors(async (req, res, next) => {
    const task = await Task.create({
        ...req.body,
        dueDate: new Date(req.body.dueDate),
    });

    if (!task) return next(new ErrorHandeler("Can't create a Task", 424));

    res.status(200).json({
        success: true,
        task,
    });
});

exports.getAllTasks = catchAssyncErrors(async (req, res, next) => {
    const tasks = await Task.find({ user: req.user._id });

    if (!tasks) return next(new ErrorHandeler("404 not found", 404));

    res.status(200).json({
        success: true,
        tasks,
    });
});

exports.taskDone = catchAssyncErrors(async (req, res, next) => {
    const _id = req.params.id;
    console.log(_id);

    const updatedTask = await Task.findByIdAndUpdate(
        { _id },
        {
            $set: {
                completed: true,
            },
            updatedAt: new Date(),
        },
        {
            multi: true,
            new: true,
            runValidators: true,
            useFindAndModify: true,
        }
    );

    if (!updatedTask) {
        return next(new ErrorHandeler("Please Reload and Try", 404));
    }

    res.status(201).json({
        success: true,
        updatedTask,
    });
});

exports.deleteTask = catchAssyncErrors(async (req, res, next) => {
    const _id = req.params.id;
    const task = await Task.findByIdAndDelete(_id);

    res.status(200).json({
        success: true,
    });
});

exports.getFilteredTasks = catchAssyncErrors(async (req, res, next) => {
    const filters = {
        ...req.query,
    };

    const query = {};

    if (filters.completed == "true") {
        query.completed = true;
    }

    if (filters.upcomming == "true") {
        query.completed = false;
        query.dueDate = {
            $gt: new Date(),
        };
    }

    if (
        filters.low == "true" ||
        filters.medium == "true" ||
        filters.high == "true"
    ) {
        const priorityFilters = [];
        if (filters.low == "true") priorityFilters.push("low");
        if (filters.medium == "true") priorityFilters.push("medium");
        if (filters.high == "true") priorityFilters.push("high");
        query.priority = { $in: priorityFilters };
    }

    const tasks = await Task.find(query);

    res.status(200).json({
        tasks,
    });
});
